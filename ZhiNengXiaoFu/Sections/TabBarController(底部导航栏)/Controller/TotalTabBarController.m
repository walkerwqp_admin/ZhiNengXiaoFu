//
//  TotalTabBarController.m
//  ZhiNengXiaoFu
//
//  Created by mac on 2018/7/20.
//  Copyright © 2018年 henanduxiu. All rights reserved.
//

#import "TotalTabBarController.h"
#import "HomePageViewController.h"
#import "ClassHomeViewController.h"
#import "QianDaoViewController.h"
#import "MineViewController.h"
@interface TotalTabBarController ()

@end

@implementation TotalTabBarController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //首页
    HomePageViewController * homePageVC  = [[HomePageViewController alloc] init];
    UINavigationController * homePageNav = [[UINavigationController alloc] initWithRootViewController:homePageVC];
    homePageVC.tabBarItem.title = @"首页";
    homePageVC.tabBarItem.image = [[UIImage imageNamed:@"foot1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    homePageVC.tabBarItem.selectedImage = [UIImage imageNamed:@""];
 

    //班级
    ClassHomeViewController * classHomeVC = [[ClassHomeViewController alloc] init];
    UINavigationController * classHomeNav = [[UINavigationController alloc] initWithRootViewController:classHomeVC];
    classHomeVC.tabBarItem.title = @"班级";
    classHomeVC.tabBarItem.image = [[UIImage imageNamed:@"foot2"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    classHomeVC.tabBarItem.selectedImage = [UIImage imageNamed:@""];

    
    //签到
    QianDaoViewController * qianDaoVC = [[QianDaoViewController alloc] init];
    UINavigationController * qianDaoNav = [[UINavigationController alloc] initWithRootViewController:qianDaoVC];
    qianDaoVC.tabBarItem.title = @"签到";
    qianDaoVC.tabBarItem.image = [[UIImage imageNamed:@"foot3"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    qianDaoVC.tabBarItem.selectedImage = [UIImage imageNamed:@""];
    
    //我
    MineViewController * mineVC = [[MineViewController alloc] init];
    UINavigationController * mineNav = [[UINavigationController alloc] initWithRootViewController:mineVC];
    mineVC.tabBarItem.title = @"我";
    mineVC.tabBarItem.image = [[UIImage imageNamed:@"foot4"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    mineVC.tabBarItem.selectedImage = [UIImage imageNamed:@""];
    
    
    [[UITabBar appearance] setBarTintColor: [UIColor colorWithRed:173/ 255.0 green:228 / 255.0 blue: 211 / 255.0 alpha:1]];
    [UITabBar appearance].translucent = NO;
    self.viewControllers = @[homePageNav, classHomeNav, qianDaoNav, mineNav];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:105 /255.0 green:99 / 255.0 blue:99 / 255.0 alpha:1]} forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateSelected];
    


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
